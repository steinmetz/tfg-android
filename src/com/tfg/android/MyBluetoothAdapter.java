package com.tfg.android;

import java.util.ArrayList;

import com.example.bluetooth.R;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MyBluetoothAdapter extends BaseAdapter {
	
	ArrayList<BluetoothDevice> foundDevices = new ArrayList<BluetoothDevice>();
	Context ctx;
	int selectedIndex = -1;
	
	public MyBluetoothAdapter(ArrayList<BluetoothDevice> foundDevices, Context ctx) {
		super();
		this.foundDevices = foundDevices;
		this.ctx = ctx;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return foundDevices.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return foundDevices.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {

		LinearLayout layout = new LinearLayout(ctx);
	    layout.setLayoutParams(new GridView.LayoutParams(
	    		android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
	            android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
	    layout.setOrientation(LinearLayout.HORIZONTAL);
	    layout.setGravity(Gravity.CENTER); 
	    ImageView img = new ImageView(ctx);
	    img.setBackgroundResource(R.drawable.bluetooth);
	    
	    
	    TextView textview = new TextView(ctx);
	    textview.setLayoutParams(new LinearLayout.LayoutParams(
	            android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
	            android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
	    textview.setText("" + foundDevices.get(arg0));
	    textview.setTextColor(Color.BLACK);

	    layout.addView(img); 
	    layout.addView(textview); 

	    return layout;
		 
	}

}
