package com.tfg.android;

public class DadosAndroidToArduino {
//	String msg = "$ln*V:velocidade;A:acao;T:tempo;S:servo#";
	public int velocidade;
	public char acao;
	public int tempo;
	public int servo;
	public DadosAndroidToArduino(int velocidade, char acao, int tempo, int servo) {
		super();
		this.velocidade = velocidade;
		this.acao = acao;
		this.tempo = tempo;
		this.servo = servo;
	}
	
	public String toArduinoString(){
		String msg = "$ln*V:velocidade;T:tempo;A:acao;S:servo#";
		msg = msg.replace("acao", String.valueOf(acao));
		msg = msg.replace("tempo",  String.valueOf(tempo));
		msg = msg.replace("velocidade",  String.valueOf(velocidade));
		msg = msg.replace("servo",  String.valueOf(servo));
		int len = msg.length();
		msg = msg.replace("ln", String.valueOf(len));
		return msg;
	}
	
	
}
