package com.tfg.android;

import java.io.IOException;
import java.io.InputStream;

import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;

public class BluetoothSocketListener implements Runnable {
	private BluetoothSocket socket;
	private TextView textView;
	private Handler handler;

	public BluetoothSocketListener(BluetoothSocket socket, Handler handler,
			TextView textView) {
		this.socket = socket;
		this.textView = textView;
		this.handler = handler;
	}

	@Override
	public void run() {
		int bufferSize = 1024;
		byte[] buffer = new byte[bufferSize];
		try {
			InputStream instream = socket.getInputStream();
			int bytesRead = -1;
			String message = "";
			while (true) {				
				if (instream.available() != 0) { 
					bytesRead = instream.read(buffer);
					while(bytesRead != -1){
						message = new String(buffer, 0, bytesRead);
						handler.post(new ProcessaMensagem(textView, message));
						bytesRead = instream.read(buffer);
					}					
					
				} else{
					message = "";
				}
			}
		} catch (IOException e) {
			Log.d("BLUETOOTH_COMMS", e.getMessage());
		}

	}
}