package com.tfg.android;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.TextView;

public class MyTextView extends TextView {

	public MyTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public MyTextView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
	}

	public MyTextView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void setText(CharSequence text, BufferType type) {
		try {
			int valor = Integer.parseInt(text.toString());
			if(valor > 50){
				this.setTextColor(Color.rgb(0,128,0));
			}else if (valor <= 450 && valor >20){
				this.setTextColor(Color.rgb(255,165,0));
			}else {
				this.setTextColor(Color.rgb(139,0,0));
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		super.setText(text, type);
	}		

}
