/*
 * 
 */
package com.tfg.android;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.UUID;

import org.apache.commons.io.FileUtils;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bluetooth.R;



// TODO: Auto-generated Javadoc
/**
 * Esta Activity é a principal do aplicativo.
 * Ela é responsável por instanciar as demais classes e executar a thread do controle.
 *
 * @author charles
 */
public class MainActivity extends Activity {

	/** The discovery request. */
	private static int DISCOVERY_REQUEST = 1;
	
	/** Objeto usado para encontrar dispositivos bluetooth. */
	private BluetoothAdapter bluetooth;
	
	/** Socket para comunicação. */
	private BluetoothSocket socket;
	
	
	private UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	
	/** Dispositivos encontrados. */
	private ArrayList<BluetoothDevice> foundDevices = new ArrayList<BluetoothDevice>();

	/** Adapter para mostrar os dispositivos. */
	private MyBluetoothAdapter aa;
	
	private ListView list;

	/** Mensagens recebidas pelo mobile. */
	static ArrayList<String> messages;
	 
	static String texto;
	
	/** Ultimo registro recebido, para verificar a duplicidade. */
	static String ultimo_registro;
	
	/** Índice do último registro. */
	int last = 0;
	
	/** Variável que controla a última curva feita. */
	String ultima_curva = ""; //sentido da última curva

	/** Thread que executa a lógica de controle. */
	Thread controle;
	
	/** The flag thread. */
	boolean flagThread = false;
	
	/** The area concluida. */
	boolean areaConcluida = false;

	/** The text_messages. */
	public TextView text_messages;
	
	/** The txt_status. */
	public TextView txt_status;

	/** The txt_ud. */
	public TextView txt_ud;
	
	/** The txt_ue. */
	public TextView txt_ue;
	
	/** The txt_uf. */
	public TextView txt_uf;
	
	/** The txt_ut. */
	public TextView txt_ut;

	/** The filename. */
	private String filename;
	
	/** The velocidade. */
	int velocidade = 255;
	
	
	/** The handler. */
	private Handler handler = new Handler();
	
	/** The message text. */
	TextView messageText;
	
	/** The text entry. */
	EditText textEntry;

	
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		texto = "";
		ultimo_registro = "";
		messages = new ArrayList<String>();

		configureBluetooth();

		setupListView();

		setupSearchButton(); 

		textEntry = (EditText) findViewById(R.id.text_message);

		text_messages = (TextView) findViewById(R.id.text_messages);
		txt_status = (TextView) findViewById(R.id.txt_status);
		
		
		filename = System.currentTimeMillis()+".txt";
	 
	}

	
 

	/**
	 * Ativa desativa thread.
	 *
	 * @param v the v
	 */
	public void ativaDesativaThread(View v) {
		if (flagThread) {
			flagThread = false;
			((Button) v).setText("Iniciar controle");
		}else{
			flagThread = true;
			((Button) v).setText("Parar controle");
		}
	}
	
	/**
	 * Comunicacao.
	 */
	public void comunicacao() {

		txt_ud = (MyTextView) findViewById(R.id.txt_ud);
		txt_ue = (MyTextView) findViewById(R.id.txt_ue);
		txt_uf = (MyTextView) findViewById(R.id.txt_uf);
		txt_ut = (MyTextView) findViewById(R.id.txt_ut);

		
		Runnable r = new Runnable() {
			@Override
			public void run() {
				while (!areaConcluida) {
					try {
						if (messages != null && messages.size() > 0
								&& last < messages.size()) {
							final DadosArduinoToAndroid dados = new DadosArduinoToAndroid(
									messages.get(messages.size() - 1));
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									txt_ud.setText(String
											.valueOf(dados.ultrassonicoDireito));
									txt_ue.setText(String
											.valueOf(dados.ultrassonicoEsquerdo));
									txt_uf.setText(String
											.valueOf(dados.ultrassonicoFrente));
									txt_ut.setText(String
											.valueOf(dados.ultrassonicoTras));
								}
							});
 
							runOnUiThread(new Runnable() {

								@Override
								public void run() {
									text_messages.setText(dados.toString());
								}
							});
							
							if(flagThread && !areaConcluida){
								if (dados.ultrassonicoFrente < 50
										&& dados.acao.equals("F")) {
									DadosAndroidToArduino d = new DadosAndroidToArduino(
											0, 'P', 0, 90);
									sendMessage(socket, d.toArduinoString());
									SystemClock.sleep(2500);
									
									
									if (ultima_curva.equals("")) {
										int direita = 0, esquerda = 0;
										DadosArduinoToAndroid recebido;
										do{
											// direita
											d = new DadosAndroidToArduino(0, 'P', 0, 180);
											sendMessage(socket, d.toArduinoString());
											SystemClock.sleep(500);
											recebido = new DadosArduinoToAndroid(getLastMessage());	
											direita = recebido.ultrassonicoFrente;
										}while(! (recebido.servo == 180));
										
										FileUtils.write(new File(filename), recebido.toString(), true);
										
										do{
											//esquerda
											d = new DadosAndroidToArduino(0, 'P', 0, 0);
											sendMessage(socket, d.toArduinoString());
											SystemClock.sleep(500);
											recebido = new DadosArduinoToAndroid(getLastMessage());
											esquerda = recebido.ultrassonicoFrente;
										}while(! (recebido.servo == 0));
										
										FileUtils.write(new File(filename), recebido.toString(), true);
										
										d = new DadosAndroidToArduino(0, 'P', 0, 90);
										sendMessage(socket, d.toArduinoString());
										
										if(dados.ultrassonicoDireito > dados.ultrassonicoEsquerdo){											
											//deveria fazer a curva para a direita, mas há limitações.
											if(dados.ultrassonicoDireito < 10 && ultima_curva.equals("E")){
												areaConcluida = true;
											}else{
												if(direita < 10){
													d = new DadosAndroidToArduino(0, 'T', 200, 180);
													sendMessage(socket, d.toArduinoString());
												}
												ultima_curva = "D";
											}
										}else{											
											//deveria fazer a curva para a direita, mas há limitações.
											if(dados.ultrassonicoEsquerdo < 10 && ultima_curva.equals("D")){
												areaConcluida = true;
											}else{
												if(esquerda > 10){
													d = new DadosAndroidToArduino(0, 'T', 200, 180);
													sendMessage(socket, d.toArduinoString());
												}
												ultima_curva = "E";
											}											
										}
									}
									
									 
									d = new DadosAndroidToArduino(255, ultima_curva.charAt(0), 2000,
											90);
									sendMessage(socket, d.toArduinoString());

									SystemClock.sleep(1000);
									d = new DadosAndroidToArduino(90, 'F', 500, 90);
									sendMessage(socket, d.toArduinoString());

									SystemClock.sleep(1000);

									d = new DadosAndroidToArduino(255, ultima_curva.charAt(0), 2000,
											90);
									sendMessage(socket, d.toArduinoString());
									SystemClock.sleep(2500);

									d = new DadosAndroidToArduino(90, 'F', 0, 90);
									sendMessage(socket, d.toArduinoString());
									SystemClock.sleep(1000);

								} else {
									
									// andar para frente enquanto não ter obstáculos.
									DadosAndroidToArduino d = new DadosAndroidToArduino(
											150, 'F', 0, 90);
									sendMessage(socket, d.toArduinoString());
								}
								
								FileUtils.write(new File(filename), dados.toString(), true);
								
							}
							
						}  
						SystemClock.sleep(200);
					} catch (Exception e) {
						Log.i("SSS", "EXC:" + e.getMessage());
					}
				}
				
				if(areaConcluida){
					Toast.makeText(getApplicationContext(), "Área concluída!", Toast.LENGTH_SHORT).show();
				}
				
				
			}
		};
		 
			controle = new Thread(r); 
			controle.start(); 
		
	}
	
	
	/**
	 * Gets the last message.
	 *
	 * @return the last message
	 */
	public String getLastMessage(){
		if(messages != null && messages.size() > 0){
			return messages.get(messages.size()-1);
		}
		return null;
	}
	

	/**
	 * Vermsgs.
	 *
	 * @param v the v
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void vermsgs(View v) throws IOException {

		System.out.println(messages);
		for (int i = 0; i < messages.size(); i++) {
			CharSequence cs = messages.get(i).toString();
			FileUtils.write(new File(Environment.getExternalStorageDirectory()
					.toString() + "/Campeiro/charles.txt"), cs, true);
		}
	}

	/**
	 * Setup list view.
	 */
	private void setupListView() {

		aa = new MyBluetoothAdapter(foundDevices, this);
		list = (ListView) findViewById(R.id.list_discovered2);
		list.setAdapter(aa);
		list.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int index,
					long arg3) {
				AsyncTask<Integer, Void, Void> connectTask = new AsyncTask<Integer, Void, Void>() {
					@Override
					protected Void doInBackground(Integer... params) {
						try {
							final BluetoothDevice device = foundDevices
									.get(params[0]);
							bluetooth.cancelDiscovery();
							socket = device
									.createRfcommSocketToServiceRecord(uuid);
							socket.connect();
							runOnUiThread(new Runnable() {

								@Override
								public void run() {
									txt_status.setText("Conectado a "
											+ device.getName() + " ["
											+ device.getAddress() + "]");
									txt_status.setTextColor(Color
											.rgb(0, 128, 0));
									((ViewManager) list.getParent())
											.removeView(list);
									LinearLayout container = (LinearLayout) findViewById(R.id.linear_simulacao_container);
									Button btn_iniciar_controle = (Button) findViewById(R.id.btn_iniciar_controle);
									View child = getLayoutInflater().inflate(
											R.layout.simulacao, null);
									container.addView(child);
									btn_iniciar_controle.setEnabled(true);
									comunicacao();
								}
							});

						} catch (IOException e) {
							Log.d("BLUETOOTH_CLIENT", e.getMessage());
						}
						return null;
					}

					@Override
					protected void onPostExecute(Void result) {

						switchUI();
					}
				};
				connectTask.execute(index);
			}
		});
	}

	

	/**
	 * Switch ui.
	 */
	private void switchUI() {
		messageText = (TextView) findViewById(R.id.text_messages);
		textEntry = (EditText) findViewById(R.id.text_message);
		final TextView messageText = (TextView) findViewById(R.id.text_messages);
		final EditText textEntry = (EditText) findViewById(R.id.text_message);
		messageText.setVisibility(View.VISIBLE);
		list.setEnabled(false);
		BluetoothSocketListener bsl = new BluetoothSocketListener(socket,
				handler, messageText);
		Thread messageListener = new Thread(bsl);
		messageListener.start();
	}

	/**
	 * On click.
	 *
	 * @param v the v
	 */
	public void onClick(View v) {
		sendMessage(socket, textEntry.getText().toString());

	}

	/**
	 * Send message.
	 *
	 * @param socket the socket
	 * @param msg the msg
	 */
	private void sendMessage(BluetoothSocket socket, String msg) {
		OutputStream outStream;
		try {
			if (socket == null) {
				Toast.makeText(getApplicationContext(), "socket null",
						Toast.LENGTH_SHORT).show();
				return;
			}
			outStream = socket.getOutputStream();
			byte[] byteString = (msg + " ").getBytes();
			byteString[byteString.length - 1] = 0;
			outStream.write(byteString);
		} catch (IOException e) {
			Log.d("BLUETOOTH_COMMS", e.getMessage());
		}
	}

	/**
	 * Configure bluetooth.
	 */
	private void configureBluetooth() {
		bluetooth = BluetoothAdapter.getDefaultAdapter();

	}

	/** The discovery result. */
	BroadcastReceiver discoveryResult = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			BluetoothDevice remoteDevice;
			remoteDevice = intent
					.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
			if (bluetooth.getBondedDevices().contains(remoteDevice)) {
				foundDevices.add(remoteDevice);
				aa.notifyDataSetChanged();
			}
		}
	};

	/**
	 * Setup search button.
	 */
	private void setupSearchButton() {
		Button searchButton = (Button) findViewById(R.id.button_search);
		searchButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				list.setEnabled(true);
				registerReceiver(discoveryResult, new IntentFilter(
						BluetoothDevice.ACTION_FOUND));
				if (!bluetooth.isDiscovering()) {
					foundDevices.clear();
					bluetooth.startDiscovery();
				}
			}
		});
	}

	

 
	/* (non-Javadoc)
	 * @see android.app.Activity#onActivityResult(int, int, android.content.Intent)
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == DISCOVERY_REQUEST) {
			boolean isDiscoverable = resultCode > 0;
			if (isDiscoverable) {
				String name = "bluetoothserver";
				try {
					final BluetoothServerSocket btserver = bluetooth
							.listenUsingRfcommWithServiceRecord(name, uuid);
					AsyncTask<Integer, Void, BluetoothSocket> acceptThread = new AsyncTask<Integer, Void, BluetoothSocket>() {
						@Override
						protected BluetoothSocket doInBackground(
								Integer... params) {

							try {
								socket = btserver.accept(params[0] * 1000);
								return socket;
							} catch (IOException e) {
								Log.d("BLUETOOTH", e.getMessage());
							}

							return null;
						}

						@Override
						protected void onPostExecute(BluetoothSocket result) {
							if (result != null)
								switchUI();
						}
					};
					acceptThread.execute(resultCode);
				} catch (IOException e) {
					Log.d("BLUETOOTH", e.getMessage());
				}
			}
		}
	}

}
