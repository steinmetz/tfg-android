package com.tfg.android;

public class DadosArduinoToAndroid {

	
	//$24*UE:0;UD:8;UF:70;S:90#
	
	public int ultrassonicoEsquerdo;
	public int ultrassonicoDireito;
	public int ultrassonicoFrente;
	public int ultrassonicoTras;
	public int servo;
	public String acao;
	
	public DadosArduinoToAndroid(String str){
		String spl[] = str.split("\\*");
		spl = spl[1].replace("#", "").split(";");
		for (String string : spl) {
			String[] spl2 = string.split(":");
			if (spl2[0].equals("UE")) {
				this.ultrassonicoEsquerdo = Integer.parseInt(spl2[1]);
			}else if (spl2[0].equals("UD")) {
				this.ultrassonicoDireito = Integer.parseInt(spl2[1]);
			}else if (spl2[0].equals("UF")) {
				this.ultrassonicoFrente = Integer.parseInt(spl2[1]);
			}else if (spl2[0].equals("UT")) {
				this.ultrassonicoTras = Integer.parseInt(spl2[1]);
			}else if (spl2[0].equals("S")) {
				this.servo = Integer.parseInt(spl2[1]);
			}else if(spl2[0].equals("A")) {
				this.acao = Character.toString ((char) Integer.parseInt(spl2[1])); 
			}
		}
	}

	@Override
	public String toString() {
		return "DadosArduinoToAndroid [ultrassonicoEsquerdo="
				+ ultrassonicoEsquerdo + ", ultrassonicoDireito="
				+ ultrassonicoDireito + ", ultrassonicoFrente="
				+ ultrassonicoFrente + ", ultrassonicoTras=" + ultrassonicoTras
				+ ", servo=" + servo + ", acao=" + acao + "]";
	}

	

	
	
	
}
