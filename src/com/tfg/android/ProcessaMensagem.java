package com.tfg.android;

import android.widget.TextView;

public class ProcessaMensagem implements Runnable {
	private TextView textView;
	private String message;

	public ProcessaMensagem(TextView textView, String message) {
		this.textView = textView;
		this.message = message;
	}

	@Override
	public void run() {
		
		String texto = MainActivity.texto + message;  
		//Log.i("SSS", texto);
		try {
			if (texto.contains("$") && texto.contains("#")) {
				String spl[] = texto.replace("$", "%$").split("\\%");
				if (MainActivity.ultimo_registro.length()<1) {
					MainActivity.ultimo_registro = spl[1];
					MainActivity.messages.add(spl[1]);
				} else{
					if (!MainActivity.ultimo_registro.equals(spl[1])) {
						MainActivity.messages.add(spl[1]);
						MainActivity.ultimo_registro = spl[1];
					}
				}
				
				if (spl.length>2) {
					texto = spl[2];
				}else{
					texto = "";
				}
			}
			 MainActivity.texto = texto;			
			
		} catch (Exception e) {
		}
	}
}
