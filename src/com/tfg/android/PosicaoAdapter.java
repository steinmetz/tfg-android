package com.tfg.android;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PosicaoAdapter extends BaseAdapter {
Context con;
ArrayList<Posicao> posicoes;

public PosicaoAdapter(Context c, ArrayList<Posicao> posicoes) {
    con = c;
    this.posicoes = posicoes;
}
@Override
public int getCount() {
    // TODO Auto-generated method stub
    return posicoes.size();
}

@Override
public Object getItem(int position) {
    // TODO Auto-generated method stub
    return posicoes.get(position);
}

@Override
public long getItemId(int position) {
    // TODO Auto-generated method stub
    return 0;
}

@Override
public View getView(int position, View convertView, ViewGroup parent) {
   LinearLayout layout = new LinearLayout(con);
    layout.setLayoutParams(new GridView.LayoutParams(
            50,
            50));
    layout.setOrientation(LinearLayout.HORIZONTAL);
    
    if(posicoes.get(position).situacao == EstadosPosicao.VISITADO)
    	layout.setBackgroundColor(Color.YELLOW);
    else if (posicoes.get(position).situacao == EstadosPosicao.OBSTACULO) {
    	layout.setBackgroundColor(Color.RED);
	} else {
		layout.setBackgroundColor(Color.GRAY);
	}
    
    
    TextView textview = new TextView(con);
    textview.setLayoutParams(new LinearLayout.LayoutParams(
            android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
            android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
//    textview.setText("TV " + position);
    textview.setTextColor(Color.BLACK);

    layout.addView(textview); 

    return layout;
}

}